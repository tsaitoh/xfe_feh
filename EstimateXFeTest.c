#include "config.h"

static int AverageElementMassSize = 0;
static struct StructAverageElementMass{
    char Label[MaxCharactersInLine];
    double Mass;
} *AverageElementMass;

static void LoadAverageElementMassData(void){

    if(AverageElementMassSize == 0){
        FILE *fp;
        FileOpen(fp,"./data/AverageMass.dat","r");

        AverageElementMassSize = 0;
        while(fscanf(fp,"%*s %*g") != EOF){
            AverageElementMassSize ++;
        }
        vprint(AverageElementMassSize);

        rewind(fp);

        AverageElementMass = malloc(sizeof(struct StructAverageElementMass)*AverageElementMassSize);

        int counter = 0;
        while(fscanf(fp,"%s %le",AverageElementMass[counter].Label,&AverageElementMass[counter].Mass) != EOF){
            // vprint(AverageElementMass[counter].Label);
            // vprint(AverageElementMass[counter].Mass);
            counter ++;
        }
        fclose(fp);
    }
    return ;

}


static int SolarAbundancePatternSize = 0;
static struct StructSolarAbudancePattern{
    char Label[MaxCharactersInLine];
    double Value;
} *SolarAbundancePattern;
static void LoadSolarAbudancePatternData(void){

    if(SolarAbundancePatternSize == 0){
        FILE *fp;
        FileOpen(fp,"./data/SolarAbundance.dat","r");

        SolarAbundancePatternSize = 0;
        while(fscanf(fp,"%*s %*g") != EOF){
            SolarAbundancePatternSize ++;
        }
        vprint(SolarAbundancePatternSize);

        rewind(fp);

        SolarAbundancePattern = malloc(sizeof(struct StructSolarAbudancePattern)*SolarAbundancePatternSize);

        int counter = 0;
        while(fscanf(fp,"%s %le",SolarAbundancePattern[counter].Label,&SolarAbundancePattern[counter].Value) != EOF){
            // vprint(SolarAbundancePattern[counter].Label);
            // vprint(SolarAbundancePattern[counter].Mass);
            counter ++;
        }
        fclose(fp);
    }

    return ;
}

static int FittingCoefSize = 0;
static struct StructFittingCoef{
    char Label[MaxCharactersInLine];
    double Coef[4];
} *FittingCoef;
static void LoadFittingCoefficientsData(void){

    if(FittingCoefSize == 0){
        FILE *fp;
        FileOpen(fp,"./data/FittingCoef.dat","r");

        FittingCoefSize = 0;
        char dummy[MaxCharactersInLine];
        fgets(dummy,MaxCharactersInLine-1,fp);
        fgets(dummy,MaxCharactersInLine-1,fp);
        while(fscanf(fp,"%*s %*g %*g %*g %*g") != EOF){
            FittingCoefSize ++;
        }
        vprint(FittingCoefSize);

        rewind(fp);

        FittingCoef = malloc(sizeof(struct StructFittingCoef)*FittingCoefSize);

        fgets(dummy,MaxCharactersInLine-1,fp);
        vprint(dummy);
        fgets(dummy,MaxCharactersInLine-1,fp);
        vprint(dummy);
        int counter = 0;
        while(fscanf(fp,"%s %le %le %le %le",FittingCoef[counter].Label,&FittingCoef[counter].Coef[0],&FittingCoef[counter].Coef[1],&FittingCoef[counter].Coef[2],&FittingCoef[counter].Coef[3]) != EOF){
            // vprint(FittingCoef[counter].Label);
            // vprint(FittingCoef[counter].Coef[0]);
            // vprint(FittingCoef[counter].Coef[1]);
            // vprint(FittingCoef[counter].Coef[2]);
            // vprint(FittingCoef[counter].Coef[3]);
            counter ++;
        }
        fclose(fp);
    }


    return ;
}

void LoadDataTest(void){

    LoadAverageElementMassData();
    LoadSolarAbudancePatternData();
    LoadFittingCoefficientsData();

    return ;
}

static int GetIndex(const char ElementName[], const int mode){

    if(mode == 0){ // Coef
        for(int i=0;i<FittingCoefSize;i++){
            if(strncmp(ElementName,FittingCoef[i].Label,MaxCharactersInLine-1) == 0){
                // vprint(ElementName);
                // vprint(FittingCoef[i].Label);
                return i;
            }
        }
        return NONE;
    } else if(mode == 1){ // AverageMass
        for(int i=0;i<AverageElementMassSize;i++){
            if(strncmp(ElementName,AverageElementMass[i].Label,MaxCharactersInLine-1) == 0){
                // vprint(ElementName);
                // vprint(AverageElementMass[i].Label);
                return i;
            }
        }
        return NONE;
    } else if(mode == 2){ // SolarAbundance
        for(int i=0;i<SolarAbundancePatternSize;i++){
            if(strncmp(ElementName,SolarAbundancePattern[i].Label,MaxCharactersInLine-1) == 0){
                // vprint(ElementName);
                // vprint(SolarAbundancePattern[i].Label);
                return i;
            }
        }
        return NONE;
    } else {
        fprintf(stderr,"Mode incorrect\n");
        assert(mode < 3);
        assert(-1 < mode);
    }
    return NONE;
}

static double FitModel(const int Index, const double Z){

    double LogZ = log10(Z);

    vprint(FittingCoef[Index].Coef[0]);
    vprint(FittingCoef[Index].Coef[1]);
    vprint(FittingCoef[Index].Coef[2]);
    vprint(FittingCoef[Index].Coef[3]);

    return FittingCoef[Index].Coef[0]+
           FittingCoef[Index].Coef[1]*LogZ+
           FittingCoef[Index].Coef[2]*SQ(LogZ)+
           FittingCoef[Index].Coef[3]*CUBE(LogZ);
}

double EstimateXFeTest(const char ElementName[], const double Z){

    int Index = GetIndex(ElementName,0);
    //vprint(ElementName);
    //vprint(Index);
    vprint(FitModel(Index,Z));
    vprint(FitModel(GetIndex(ElementName,0),Z));

    if(Index == NONE)
        return NONE;

    // Get nB
    //    nB = Z*10.0^(GetSolarAbudance("Fe")-12.0)
    double nB = Z*pow(10.0,SolarAbundancePattern[GetIndex("Fe",2)].Value-12.0);

    // Get nAnB_sun
    //nAnB_sun = 10.0^(GetSolarAbudance(Element)-12.0)/10.0^(GetSolarAbudance("Fe")-12.0)
    double nAnB_sun = pow(10.0,(SolarAbundancePattern[GetIndex(ElementName,2)].Value-12.0))/
                      pow(10.0,(SolarAbundancePattern[GetIndex("Fe",2)].Value-12.0));

    //return nB*nAnB_sun*10.0^(XFe)
    vprint((nB/Z)*nAnB_sun);
    return nB*nAnB_sun*pow(10.0,FitModel(GetIndex(ElementName,0),Z));
}
