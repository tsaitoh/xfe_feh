#include "config.h"
#include "EstimateXFeTest.h"
#include "EstimateXFe.h"

int main(const int argc, const char **argv){

    LoadDataTest();
    vprint(EstimateXFeTest("C",1));
    vprint(EstimateXFe("C",1,XFeFeH_SAGAFull));
    // vprint(EstimateXFeTest("N",1));
    // vprint(EstimateXFe("N",1));
    // vprint(EstimateXFeTest("O",1));
    // vprint(EstimateXFe("O",1));
    vprint(EstimateXFeTest("Eu",1));
    //vprint(EstimateNX("Eu",1));
    vprint(EstimateMassX("Eu",1,XFeFeH_SAGAFull));
    //vprint(EstimateXFe("Au",1));

    double Elements[13];
    vprint("Start Saga full data with Z=Zsun");
    EstimateAbudancePattern(Elements,1,XFeFeH_SAGAFull);
    vprint("Start Saga full data with Z=0.1*Zsun");
    EstimateAbudancePattern(Elements,0.1,XFeFeH_SAGAFull);
    vprint("Start Saga full data with Z=2*Zsun");
    EstimateAbudancePattern(Elements,2,XFeFeH_SAGAFull);


    EstimateAbundancePatternTest(XFeFeH_SAGAFull);

    vprint("Start Apogee data with Z=Zsun");
    EstimateAbudancePattern(Elements,1,XFeFeH_APOGEE);
    vprint("Start Apogee data with Z=0.1*Zsun");
    EstimateAbudancePattern(Elements,0.1,XFeFeH_APOGEE);
    vprint("Start Apogee data with Z=2*Zsun");
    EstimateAbudancePattern(Elements,2,XFeFeH_APOGEE);

    EstimateAbundancePatternTest(XFeFeH_SAGACompact);
    EstimateAbundancePatternTest(XFeFeH_GALAH);
    EstimateAbundancePatternTest(XFeFeH_GALAH_APOGEE);
    EstimateAbundancePatternTest(XFeFeH_GALAH_SAGAFull);
    EstimateAbundancePatternTest(XFeFeH_APOGEE);

    return EXIT_SUCCESS;
}
