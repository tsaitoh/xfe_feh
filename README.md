# XFe_FeH

This program estimate [X/Fe]-[Fe/H]. The relations are based on the output of the SAGA database.
This covers 16 elements (C,N,O,Ne,Na,Mg,Si,P,S,Ca,Fe,Ni,Zn,Ba,Sr,Eu) and the metallicity range of -3 < [Fe/H] < 0.5.
Note that

* there is no observed data of Ne. This program assumes that [Ne/Fe] is the same as [Mg/Fe].
* there are a few observaed data of P. [P/Fe] is also 0 for the all range.

The solar abundance pattern of Asplund et al. 2009 is assumed through the program.

## Primary APIs

### Estimate XFe
```
double EstimateXFe(const char ElementName[], const double FeH);
```
This function returns the value of [X/Fe]. The first parameter is the element name and the second parameter is the value of [Fe/H].
When this function gets a wrong element name, this function will terminate by calling assert().

### EstimateNX
```
double EstimateNX(const char ElementName[], const double FeH);
```
This function returns the relative number of the element. The hydrogen number is normalized as unity.
If an uncovered element name is given, the program returns -1.

### EstimateMassX
```
double EstimateMassX(const char ElementName[], const double FeH);
```
This function returns the relative mass of the element. The hydrogen mass is 1 times average_mass_of_hydrogen.
If an uncovered element name is given, the program returns -1.

### Example: set abundance pattern at a given [Fe/H]
```
void EstimateAbudancePattern(double Elements[], const double FeH)
```
This function fills the array `Elements` so that all the hearvy elements follow the observed relation.
Here this function is assumed the CELib standard elements, but can easily extend to the others.
Note that the helium abundance is fixed to the solar abundance pattern. 
In this function, X+Y+Z=1.

### Example: set abundance pattern at a given [Fe/H] for an SSP particle.
```
void EstimateAbudancePatternForMass(double Elements[], const double FeH, Mass)
```
This is a variant of `EstimateAbudancePattern`. This function is X+Y+Z=Mass. So, it is easy to use for simulations of galaxy formation.

